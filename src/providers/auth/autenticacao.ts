import { Injectable } from "@angular/core";
import { ServicoProvider } from "../servico/servico";
import { Http } from "@angular/http";
import { Storage } from "@ionic/storage";
import { ENV } from "../../environments/environment";
import { Facebook } from "@ionic-native/facebook";
import { GooglePlus } from "@ionic-native/google-plus";
import { NavController } from "ionic-angular";
import { HomePage } from "../../pages/home/home";



@Injectable()
export class AutenticacaoProvider extends ServicoProvider {
    usuario;
    constructor(
        public http: Http,
        public storage: Storage,
        private fb: Facebook,
        private googlePlus: GooglePlus,

    ) {
        super(http)
    }

    public estaAutenticado(): Promise<boolean> {

        return this.storage.get(ENV.authetication.profilekeyName);
    }

    public async login(email: string, senha: string) {
        const response = await this.post('api/Usuarios/Login?email=' + email + '&' + 'senha=' + senha).toPromise();

        const retorno = response.json();

        this.storage.set(ENV.authetication.profilekeyName, retorno.Id);

        return retorno;
    }

    public loginFb() {
        return this.fb.login(['public_profile', 'email'])
            .then(res => {
                if (res.status === "connected") {
                    return this.pegaDadosUsuarioFb(res.authResponse.userID);
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));

    }

    pegaDadosUsuarioFb(userid) {
        return this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(usuario => {
                return usuario;
                //this.storage.set(ENV.authetication.profilekeyName, usuario.id);
            })
            .catch(e => {
                console.log(e);
            });
    }

    public loginGoogle() {
        return this.googlePlus.login({})
            .then(usuarioGoogle => {
                console.log(usuarioGoogle)
                return usuarioGoogle;
            })
            .catch(err => console.error(err));
    }

    public async logout() {

        return this.storage.clear();
    }



}