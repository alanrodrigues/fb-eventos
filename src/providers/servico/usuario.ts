import { ServicoProvider } from "./servico";

export class UsuarioProvider extends ServicoProvider{
    public async cadastrarUsuario(usuario){
    
        const response = await this.post('api/Usuarios', usuario).toPromise();

        const retorno = response.json();

        return retorno;
    }

}