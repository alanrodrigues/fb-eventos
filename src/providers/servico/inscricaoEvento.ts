import { Injectable } from "@angular/core";
import { ServicoProvider } from "./servico";


@Injectable()
export class EventoProvider extends ServicoProvider {
    

       public async postInscricaoEvento({inscricao}) {
        return await this.post('api/InscricaoEventos/',inscricao);
    }

}