import { Injectable } from "@angular/core";
import { ServicoProvider } from "./servico";

@Injectable()
export class EventoProvider extends ServicoProvider {
    public async getEventos() {
        return await this.get('api/Evento');
    }
     public async getEventosId(idEvento) {
        return await this.get('api/Evento/'+idEvento);
    }

       public async postEvento({evento}) {
        return await this.post('api/Evento/',evento);
    }

}