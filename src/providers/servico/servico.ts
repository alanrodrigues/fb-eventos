import { RequestOptions, Http, RequestOptionsArgs } from "@angular/http";
import { ENV } from "../../environments/environment";
import { Injectable } from "@angular/core";
;
interface RetornoWebService {
    retorno: any,
    exception?: {
        errorCode: number,
        errorMessage: string
    }
}


@Injectable()
export abstract class ServicoProvider {
    constructor(
        public http: Http) { }


    public post(url: string, body?: any, options?: RequestOptions) {
  
        return this.http.post(ENV.webServices.fbEventoServices.baseUrl + url, body);
    }

    public async get(url: string, options?: RequestOptionsArgs) {
        const response = await this.http.get(ENV.webServices.fbEventoServices.baseUrl + url).toPromise();
        console.log(ENV.webServices.fbEventoServices.baseUrl + url)
        const retornoWebService: RetornoWebService = response.json();

        if (retornoWebService.exception != null) {
            //throw new Error(retornoWebService.exception.errorMessage)
        }

        return retornoWebService;
    }


}