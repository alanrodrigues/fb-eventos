import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { GooglePlus } from '@ionic-native/google-plus';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { Facebook } from '@ionic-native/facebook';
import { LoginPage } from '../pages/login/login';
import { AutenticacaoProvider } from '../providers/auth/autenticacao';
import { EventoProvider } from '../providers/servico/eventos';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { DetalhesEventosPage } from '../pages/detalhes-eventos/detalhes-eventos'
import { PagesCadastroUsuarioPage } from '../pages/pages-cadastro-usuario/pages-cadastro-usuario';
import { CadastroEventoPage } from '../pages/cadastro-evento/cadastro-evento';

import { OneSignal } from '@ionic-native/onesignal';
import { UsuarioProvider } from '../providers/servico/usuario';
import { SocialSharing } from '@ionic-native/social-sharing';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DetalhesEventosPage,
    PagesCadastroUsuarioPage,
    CadastroEventoPage,
 
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({ name: 'banco' })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    DetalhesEventosPage,
    PagesCadastroUsuarioPage,
    CadastroEventoPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    EventoProvider,
    AutenticacaoProvider,
    GooglePlus,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Facebook,
    EventoProvider,
    OneSignal,
    UsuarioProvider,
    SocialSharing

  ]
})
export class AppModule { }
