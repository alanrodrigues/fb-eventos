import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController, Nav, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { AutenticacaoProvider } from '../providers/auth/autenticacao';
import { OneSignal } from '@ionic-native/onesignal';



@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  signal_app_id: string = '1066188e-ea91-4467-a37f-d8e9b10012ba';
  firebase_id: string = '1036724462231';
  rootPage: any;
  @ViewChild('nav') nav: Nav;
  public contador= 0;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public storage: Storage,
    private autenticacaoProvider: AutenticacaoProvider,
    public alertCtrl: AlertController,
    private oneSignal: OneSignal,
    private toastCtrl: ToastController,
    private app: App) {
    // this.autenticacaoProvider.estaAutenticado().then(estaAutenticado => {
    //   this.rootPage = estaAutenticado ? HomePage : LoginPage;
    // });
    this.rootPage = HomePage;
    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString("#ffffff");
      splashScreen.hide();

      // this.oneSignal.startInit(this.signal_app_id, this.firebase_id);
      // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
      // this.oneSignal.handleNotificationReceived().subscribe(() => {
      // });
      // this.oneSignal.handleNotificationOpened().subscribe(() => {
      // });

      // this.oneSignal.endInit();


    });

    platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNavs()[0];
      let active = nav.getActive();

      if (active.instance instanceof LoginPage || active.instance instanceof HomePage) {
        if(this.contador == 0){
          this.contador++;
          this.presentToast();
          setTimeout(() => {this.contador = 0}, 3000)
        }else{
          platform.exitApp();
        }
      }
    }, 0);

  }

  public presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Clique novamente para sair',
      duration: 3000,
      position: "bottom"
    });

    toast.present();
  }

  public sair() {
    const confirm = this.alertCtrl.create({
      title: 'Deseja realmente sair?',
      buttons: [
        "Não",
        {
          text: 'Sim',
          handler: () => {
            this.executarLogout();
          }
        }
      ]
    });
    confirm.present();
  }

  public async executarLogout() {
    await this.autenticacaoProvider.logout();

  }
}

