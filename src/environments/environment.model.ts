export interface Environment{
    production: boolean,
    authetication: {
        acessTokenName: string,
        profilekeyName: string
    },
    webServices: {
        [nomeServico: string]: {
            baseUrl: string,
        }
    }
}