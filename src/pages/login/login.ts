import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, MenuController } from 'ionic-angular';

import { AutenticacaoProvider } from '../../providers/auth/autenticacao';
import { HomePage } from '../home/home';
import { PagesCadastroUsuarioPage } from '../pages-cadastro-usuario/pages-cadastro-usuario';
import { Usuario } from '../../model/usuario.model';
import { Storage } from '@ionic/storage';
import { ENV } from '../../environments/environment';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public autentica;
  public usuario: Usuario;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public autenticacao: AutenticacaoProvider,
    public menu: MenuController,
    private storage: Storage,
    public toastCtrl: ToastController) {
    this.menu.enable(false)
  }

  async loginFB() {

    let usuarioFb = await this.autenticacao.loginFb();
    if (usuarioFb == null) {
    }else{
      this.menu.enable(true)
      this.navCtrl.setRoot(HomePage, { usuario: usuarioFb });
      this.storage.set(ENV.authetication.profilekeyName, usuarioFb.id);
    }

  }

  async loginGoogle() {

    let usuarioGoogle = await this.autenticacao.loginGoogle();
    if(usuarioGoogle == null){
      console.log(usuarioGoogle," Erro")
    }else{
      this.menu.enable(true)
      this.navCtrl.setRoot(HomePage, {usuario: usuarioGoogle})
      this.storage.set(ENV.authetication.profilekeyName, usuarioGoogle.userId)
    }
    

  }


  entrar(email, senha) {
    // this.autentica = await this.autenticacao.login(email, senha).then(a => {
    //   console.log(a)
    //   this.navCtrl.setRoot(HomePage);
    // }).catch(error => {
    //   console.log(error)
    // })
    // this.usuario = this.eventMock.loginFake(email, senha);
    // if (this.usuario == null) {
    //   this.toast('Dados incorretos');
    // } else {
    //   this.menu.enable(true)
    //   this.storage.set(ENV.authetication.profilekeyName, this.usuario[0].idUsuario);
    //   this.navCtrl.setRoot(HomePage, { usuario: this.usuario });
    // }

  }


  toast(mensagem) {
    const toast = this.toastCtrl.create({
      message: mensagem,
      duration: 3000
    });
    toast.present();
  }



  cadastro() {
    this.navCtrl.setRoot(PagesCadastroUsuarioPage)
  }

}
