import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { EventoProvider } from '../../providers/servico/eventos';
import { Eventos } from '../../model/eventos.model';

import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-detalhes-eventos',
  templateUrl: 'detalhes-eventos.html',
})
export class DetalhesEventosPage {
  
  public detalhesEvento: any;
  evento: Eventos;
  resultados;

  message;
  file;
  link;
  subject;

  constructor(public navCtrl: NavController,
    public eventosProvider: EventoProvider,
    public navParams: NavParams,
   private socialSharing: SocialSharing
  ) { 
  }

  compartilharEvento()
  {
    this.socialSharing.share(this.message, this.subject, this.file, this.link).then((a) => {
      console.log(a);
    })
  }

    async ionViewDidLoad() {
    let idEvento = this.navParams.get('idEvento');
    this.detalhesEvento = await this.eventosProvider.getEventosId(idEvento);
    console.log(this.detalhesEvento)
    this.evento = this.detalhesEvento;
   
  }

  inscrever(){

    
  }



}
