import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, Loading } from 'ionic-angular';
import { CadastroEventoPage } from '../cadastro-evento/cadastro-evento';
import { UsuarioProvider } from '../../providers/servico/usuario';
import { Usuario } from '../../model/usuario.model';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-pages-cadastro-usuario',
  templateUrl: 'pages-cadastro-usuario.html',
})
export class PagesCadastroUsuarioPage {


  public formUsuario: FormGroup;
  public nome: AbstractControl;
  public email: AbstractControl;
  public telefone: AbstractControl;
  public senha: AbstractControl;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public loadCtrl: LoadingController,
    public usuarioProvider: UsuarioProvider,
    public _FB: FormBuilder) {
    this.formUsuario = this._FB.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      telefone: ['', Validators.required],
      senha: ['', Validators.required]
    });

    this.nome = this.formUsuario.controls['nome'];
    this.email = this.formUsuario.controls['email'];
    this.telefone = this.formUsuario.controls['telefone'];
    this.senha = this.formUsuario.controls['senha'];

  }

  async cadastrarUsuario() {
    if (!this.formUsuario.valid) {
      const toast = this.toastCtrl.create({
        message: 'Por favor, insira todos os dados',
        duration: 3000
      });

      await toast.present();
      return;
    }

    let loading = this.loadCtrl.create({
      content: 'Carregando'
    });

    await loading.present();

    try {
      let data = await this.usuarioProvider.cadastrarUsuario(this.formUsuario.value);
      console.log(data);
      await this.sucessoCadastrar(data, loading);
    } catch (e) {
      await this.erroCadastrar(e, loading);
    }

  }

  private async sucessoCadastrar(data: any, loading: Loading) {
    await loading.dismiss();
    this.navCtrl.setRoot(HomePage, { usuario: data });
  }

  private async erroCadastrar(e: any, loading: Loading) {
    await loading.dismiss();
    console.log("Erro " + e)
    const toast = this.toastCtrl.create({
      message: 'Não foi possível cadastrar',
      duration: 3000
    });

    await toast.present();
  }
}
