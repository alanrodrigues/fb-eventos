import { Component } from '@angular/core';
import { NavController, MenuController, NavParams } from 'ionic-angular';
import { EventoProvider } from '../../providers/servico/eventos';
import { DetalhesEventosPage } from '../detalhes-eventos/detalhes-eventos';
import { Storage } from '@ionic/storage';
import { ENV } from '../../environments/environment';
import { LoginPage } from '../login/login';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  resultados;
  img;
  user;

  constructor(
    public navCtrl: NavController,
    public eventosProvider: EventoProvider,
    public storage: Storage,
    public navParams: NavParams,
    public menu: MenuController,
  ) { }

  async ionViewDidLoad() {
    this.user = this.navParams.get('usuario');
    // this.user.img = this.user.imageUrl;
    // if (this.user == null) {
    //   return;
    // }

    this.resultados = await this.eventosProvider.getEventos();
    console.log(this.resultados)
    // this.img = await this.storage.get(ENV.authetication.acessTokenName);
    // this.user = await this.storage.get(ENV.authetication.profilekeyName);
  }

  async filtrar(valor) {
    this.resultados = await this.eventosProvider.getEventos();
    const val = valor.target.value;
    if (val && val.trim() != '') {
      this.resultados = this.resultados.filter((item) => {
        return (item.Nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  public diaSemana(dataRecebida) {

    let dataFormatada = dataRecebida.split("T");

    let novaData = dataFormatada[0].split("-");
    let ano = novaData[0];
    let mes = novaData[1];
    let dia = novaData[2];

    let dias = new Array("Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb");

    let data = new Date(ano, mes, dia);

    return dias[data.getDay()];
  }

  public retornaDia(dataRecebida) {
    let a = dataRecebida.split("T");

    let data = a[0];
    let dataFormatada = data.split("-");
    let dia = dataFormatada[2];

    return dia;
  }

  public retornaMes(dataRecebida) {
    let dataFormatada = dataRecebida.split("T");

    let novaData = dataFormatada[0].split("-");
    let mes = novaData[1];
    let meses = new Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

    return meses[mes.substring(1)];

  }

  public async detalhesEvento(id) {
    this.navCtrl.push(DetalhesEventosPage, { idEvento: id })
    // if (this.user == null) {
    //   this.paginaLogin();
    // } else {
    //   this.navCtrl.push(DetalhesEventosPage, { idEvento: id })
    // }
  }

  public pequenaDescricao(valor): string {
    let formatado = valor.split(",");
    return formatado[0];
  }

  paginaLogin() {
    this.navCtrl.setRoot(LoginPage);
  }

}
