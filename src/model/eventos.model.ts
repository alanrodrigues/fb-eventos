export class Eventos{
    public idEvento: number;
    public idUsuario: number;
    public nome: string;
    public descricao: string;
    public LocalizacaoX: string;
    public LocalizacaoY: string;
    public eventoFB: number;
    public dataEventoInicio: string;
    public dataEventoFim: string;
    public toleranciaInicio: number;
    public toleranciaFinal: number;
    public quantidadeDeVagas: number;
    public confirmacaoInscricao:number;
}